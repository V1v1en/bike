<?php require_once('../private/initialize.php'); 
include(SHARED_PATH . '/public_header.php');?>
<div id="page">
    <div class="intro">
    <img src="" alt="" srcset="">
    <h2>Mes vélos</h2>
    <p>lorem ipsum dolor sit amet</p>
    </div>
    <table>
        <tr>
            <th>Marque</th>
            <th>Modèle</th>
            <th>Année</th>
            <th>Catégorie</th>
            <th>Genre</th>
            <th>Couleur</th>
            <th>Poids</th>
            <th>État</th>
            <th>Prix</th>
        </tr>
        <?php 
        $sql = "SELECT * FROM bikes";
        if ($result = $database->query($sql)) {
            /* Récupère un tableau associatif */
            $bikes = array_slice($result->fetch_assoc(), 1);
            ?>
            <tr>
            <?php
            foreach($bikes as $bike) { ?>
                    <td><?php printf($bike); ?></td>
            <?php } ?>
            </tr>
        
            <!-- /* Libération des résultats */ -->
            <!-- $result->free(); -->
        <?php } ?>
    </table>
</div>
<?php include(SHARED_PATH . '/public_footer.php') ?>