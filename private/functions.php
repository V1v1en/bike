<?php

function error_404(){
    header($_SERVER["SERVER_PROTOCOL"] . "404 not found");
    exit();
}

function error_500(){
    header($_SERVER["SERVER_PROTOCOL"] . "server error");
    exit();
}

function redirect_to($location){
    header('Location: ' . $location);
    exit();
}

function url_for($script_path){
    if($script_path[0] != '/'){
        $script_path = '/' . $script_path;
    }
    return $script_path;
}


